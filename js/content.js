// script.js
const mainLink = document.querySelector('a[href="main.html"]');
const aboutLink = document.querySelector('a[href="about.html"]');
const contentContainer = document.getElementById('content');

mainLink.addEventListener('click', function(event) {
  event.preventDefault();
  loadContent('main.html');
});

aboutLink.addEventListener('click', function(event) {
  event.preventDefault();
  loadContent('about.html');
});

function loadContent(page) {
  fetch(page)
    .then(response => response.text())
    .then(content => {
      contentContainer.innerHTML = content;
    })
    .catch(error => console.error('Error loading content:', error));
}

